<?php
/**
 *
 */


namespace WPezSuite\WCezClasses\StorefrontSetMods;


// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'ClassStorefrontSetMods' ) ) {
	class ClassStorefrontSetMods implements InterfaceStorefrontSetMods {

		protected $_arr_values;
		protected $_arr_defaults;
		protected $_arr_key_defaults;
		protected $_str_key_default;

		public function __construct() {

			$this->setPropertyDefaults();

		}

		protected function setPropertyDefaults() {

			$this->_arr_values       = [];
			$this->_arr_defaults     = [];
			$this->_arr_key_defaults = [ 'both', 'def', 'val' ];
			$this->_str_key_default  = 'both';

		}

		public function getDefaults() {

			return $this->_arr_defaults;
		}

		/**
		 * The key defines what theme mod you want to set: 'def' = default, 'val' = value, 'both' = both
		 *
		 * @param string $str
		 *
		 * @return bool
		 */
		public function setKeyDefault( $str = 'both' ) {

			if ( is_string( $str ) ) {
				$str = trim( strtolower( $str ) );
				if ( in_array( $str, $this->_arr_key_defaults ) ) {
					$this->_str_key_default = $str;

					return true;
				}
			}

			return false;
		}


		public function loadMods( $arr_mods = false ) {


			if ( is_array( $arr_mods ) ) {

				// TODO - make this a property
				$arr_exceptions = [ 'layout' ];

				// TODO add $arr_results to keep track of returns / errors, etc.
				foreach ( $arr_mods as $str_mod => $mix ) {

					// is it an array and is it a mod that accepts arrays?
					if ( is_array( $mix ) && ! in_array( $str_mod, $arr_exceptions ) ) {

						if ( isset( $mix['active'] ) && $mix['active'] === false ) {
							continue;
						}

						$str_mod_fix = trim( strtolower( $str_mod ) );
						// add 'storefront_' prefix to add mods sans 'background_color
						if ( $str_mod_fix !== 'background_color' ) {
							$str_mod_fix = 'storefront_' . $str_mod_fix;
						}

						if ( isset( $mix['both'] ) && is_string( $mix['both'] ) ) {

							$this->setColorMaster( $mix['both'], 'both', $str_mod_fix );

						} else {
							if ( isset( $mix['def'] ) && is_string( $mix['def'] ) ) {

								$this->setColorMaster( $mix['def'], 'def', $str_mod_fix );

							}
							if ( isset( $mix['val'] ) && is_string( $mix['val'] ) ) {
								$this->setColorMaster( $mix['val'], 'val', $str_mod_fix );
							}
						}

					} elseif ( is_string( $mix ) ) {

						switch ( $str_mod ) {

							case 'layout':
								$this->setLayout( $mix );
								break;

							case 'background_color':
								$str_hex = ltrim( $mix, '#' );
								$this->setColorMaster( $str_hex, 'both', 'background_color' );
								break;

							default:
								$this->setColorMaster( $mix, 'both', 'storefront_' . trim( strtolower( $str_mod ) ) );

						}

					}

				}

				return false;

			}
		}


		/**
		 *
		 * =========== Background
		 *
		 */

		public function setColorBackground( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'background_color' );

		}


		/**
		 *
		 * =========== Buttons
		 *
		 */

		public function setColorButtonBackground( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_button_background_color' );

		}

		public function setColorButtonText( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_button_text_color' );

		}

		public function setColorButtonBackgroundAlt( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_button_alt_background_color' );

		}

		public function setColorButtonTextAlt( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_button_alt_text_color' );

		}


		/**
		 *
		 * =========== Footer
		 *
		 */

		public function setColorFooterBackground( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_footer_background_color' );

		}

		public function setColorFooterHeading( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_footer_heading_color' );

		}

		public function setColorFooterLink( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_footer_link_color' );

		}

		public function setColorFooterText( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_footer_text_color' );

		}

		/**
		 *
		 * =========== Header
		 *
		 */


		public function setColorHeaderBackground( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_header_background_color' );

		}

		public function setColorHeaderLink( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_header_link_color' );

		}

		public function setColorHeaderText( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_header_text_color' );

		}

		/**
		 *
		 * =========== Hero
		 *
		 */

		public function setColorHeroHeading( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_hero_heading_color' );

		}

		public function setColorHeroText( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_hero_text_color' );

		}

		/**
		 *
		 * =========== Typography
		 *
		 */

		public function setColorAccent( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_accent_color' );

		}

		public function setColorHeading( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_heading_color' );

		}

		public function setColorText( $str_hex = false, $str_key = false ) {

			return $this->setColorMaster( $str_hex, $str_key, 'storefront_text_color' );

		}

		/**
		 *
		 * =========== Layout
		 *
		 */

		public function setLayout( $str = 'right' ) {

			$str_def = 'right';
			if ( trim( strtolower( $str ) ) === 'left' ) {
				$str_def = 'left';
			}

			$this->_arr_values['storefront_layout'] = $str_def;

			return true;
		}

		/**
		 *
		 * =========== Product Page = TODO
		 *
		 */

		/**
		 *
		 * =========== Menus = TODO
		 *
		 */


		/**
		 *
		 * =========== WooCommerce = TODO
		 *
		 */

		// -------------------------------------------


		protected function setColorMaster( $str_value = '', $str_key = false, $str_name = '' ) {

			$str_key = trim( strtolower( $str_key ) );

			if ( ! in_array( $str_key, $this->_arr_key_defaults ) ) {
				$str_key = $this->_str_key_default;
			}

			$str_value = $this->validateHex( $str_value );

			if ( ! is_string( $str_value ) ) {
				return false;
			}

			if ( $str_name == 'background_color' ) {
				$str_value = ltrim( $str_value, '#' );
			}

			switch ( $str_key ) {

				case 'def':
					return $this->setColorDefault( $str_name, $str_value );

				case 'val':

					return $this->setColorValue( $str_name, $str_value );

				case 'both':
					$this->setColorDefault( $str_name, $str_value );
					$this->setColorValue( $str_name, $str_value );

					return true;

				default:
					return false;

			}
		}

		protected function setColorValue( $str_name = '', $str_value = '' ) {

			$this->_arr_values[ $str_name ] = $str_value;

			return true;

		}


		protected function setColorDefault( $str_name = '', $str_value = '' ) {


			$this->_arr_defaults[ $str_name ] = $str_value;

			return true;
		}

		public function register() {

			foreach ( $this->_arr_values as $str_name => $str_value ) {

				set_theme_mod( $str_name, $str_value );
			}
		}

		public function filterStorefrontSettingDefaultValues( $arr_defs ) {

			return array_merge( $arr_defs, $this->_arr_defaults );
		}


		protected function validateHex( $str_value ) {

			$str_value2 = ltrim( $str_value, '#' );

			if ( preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $str_value2 ) ) {
				// TODO - prefix with # is it doesn't exist
				return $str_value;
			}

			return false;
		}

	}
}