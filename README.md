## WCezClasses: WooCommerce Storefront Set Mods

__Sidestep the WordPress theme customizer in order to set Storefront's defaults and/or values The ezWay.__

This class allows you to set the default value, set the mod value, or set both with the same value at the same time. 

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Examples 

_Recommended: Use the WCezClasses autoloader (link below)._

```
use WPezSuite\WCezClasses\StorefrontSetMods\ClassStorefrontSetMods as SSM;
use WPezSuite\WCezClasses\StorefrontSetMods\ClassHooks as Hooks;

$new = new SSM();

$red = '#BF0D3E';
$white = '#FFFFFF';
$blue = '#041E42;';
    

// set 'def' and 'val' to red
$new->setColorBackground($red)
// or 
$new->setColorBackground($red, 'both');

// set 'def' to red and 'val' to blue
$new->setColorBackground($red, 'def');
$new->setColorBackground($blue, 'val');

// set multiple defs - without having to add 'def' each time
$new->setKeyDefault('def');
$new->setColorBackground($red);
$new->setColorHeading($white);
$new->setColorAccent($blue);

// set multiple val - without having to add 'val' each time
$new->setKeyDefault('val');
$new->setColorBackground($blue);
$new->setColorHeading($white);
$new->setColorAccent($red);

$new_hooks = new Hooks($new);
$new_hooks->register();

```

Or - You can also do all your configuring via an array and use the loadMods() method.

```

use WPezSuite\WCezClasses\StorefrontSetMods\ClassStorefrontSetMods as SSM;
use WPezSuite\WCezClasses\StorefrontSetMods\ClassHooks as Hooks;

$red = '#BF0D3E';
$white = '#FFFFFF';
$blue = '#041E42;';
        
$arr_mods = [
        
    // sets both def and val to $red
    'background_color' => $red,
    
    // ----------
    // Important!
    // No need for the 'storefront_' prefix, the class will take care of that

    // sets layout to right
    'layout' => 'right',
            
    // sets both def and val to $blue
    'button_background_color' => $blue,
    
    // you can also set both explicitly like this
    'button_text_color' => [
        'both' => $white
    ],
    
    // def is set to $red, and $val to $white
    'button_alt_background_color' => [
        'def' => $red,
        'val' => $white
    ],
    
    // if by chance you do something like this,
    // both is evaluated first, and anything else is skipped
    // that is, the def and val will not be set independently
    // they will be ignored  
    'button_alt_text_color' => [
        'both' => $red,
        'def' => $red,
        'val' => $white
    ],
    
    // you can use the active flag to "turn off" a setting
    'footer_background_color' => [
        'active' => false,
        'val' => $white
    ]
        
];
    
    $new = new SSM();
    $new->loadMods($arr_mods);
    $new_hooks = new Hooks($new);
    $new_hooks->register();

```

### FAQ

**1) Why?**

Because configuring > coding.

Ultimately, arrays are so much easier to work with. If it helps, think of each script as an object with a finite handful of properties. Objects and properties are now ez to update, etc.

If you're concerned about "unauthorized changes" to your design's color scheme (that clients are fond of making) then you've come to the right place.


**2) Can I use this in my plugin or theme?**

Yes, but to be safe, please change the namespace. 


**3) Nice work. Can we hire you?**

 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 




### HELPFUL LINKS

 - 

### TODO

- Provide a better example(s)



### CHANGE LOG

- v0.0.5 - 13 May 2019
    - REFACTORED: now using the The ezWay's interface / hooks pattern

- v0.0.4 - 18 April 2019
  - FIXED: namespace (again)

- v0.0.3 - 4 April 2019
  - FIXED: namespace

- v0.0.2 - 20 March 2019
  - ADDED: loadMods() method
   

- v0.0.1 - 19 March 2019
  - Hey! Ho!! Let's go!!! 
   