<?php

namespace WPezSuite\WCezClasses\StorefrontSetMods;


interface InterfaceStorefrontSetMods {

	public function register();

	public function filterStorefrontSettingDefaultValues($arr_defs);

}
